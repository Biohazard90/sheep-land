
var sounds = {};
window.AudioContext = window.AudioContext||window.webkitAudioContext;
var context = new AudioContext();

var _executePlayJSSound = function(buffer, panning)
{
	var source = context.createBufferSource();
	source.buffer = buffer;   

	var panner = context.createPanner();
	source.connect(panner);
	//source.connect(context.destination);
	panner.panningModel = 'HRTF';
	panner.distanceModel = 'inverse';
	panner.refDistance = 1;
	panner.maxDistance = 10000;
	panner.rolloffFactor = 1;
	panner.coneInnerAngle = 360;
	panner.coneOuterAngle = 0;
	panner.coneOuterGain = 0;
	panner.setOrientation(1,0,0);
	panner.setPosition(panning,0,0.5);
	var listener = context.listener;
	listener.setOrientation(0,0,-1,0,1,0);    

	panner.connect(context.destination);
	source.start(0);
};

var playJSSound = function(soundname, panning)
{
	if (sounds[soundname]) {
		_executePlayJSSound(sounds[soundname], panning);
		return;
	}
	var request = new XMLHttpRequest();
	request.open('GET', soundname, true);
	request.responseType = 'arraybuffer';

	// Decode asynchronously
	request.onload = function() {
		context.decodeAudioData(request.response, function(buffer) {
			sounds[soundname] = buffer;
		  _executePlayJSSound(buffer, panning);
		  
		});
	}
	request.send();
};