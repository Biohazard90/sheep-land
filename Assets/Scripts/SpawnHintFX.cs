﻿using UnityEngine;
using System.Collections;

public class SpawnHintFX : MonoBehaviour
{

    public ParticleSystem Particles;

    private bool hasPlayed;

    public void StartFX()
    {
        if (!hasPlayed)
        {
            Particles.Play();
            hasPlayed = true;
        }
    }

    public void StopFX()
    {
        Particles.Stop();
        hasPlayed = true;
    }
}
