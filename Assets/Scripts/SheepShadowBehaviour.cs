﻿using UnityEngine;
using System.Collections;

public class SheepShadowBehaviour : MonoBehaviour
{

    private SpriteRenderer spriteRenderer;
    private SheepBehaviour sheepBehaviour;

    public GameObject sheep;
    public Sprite[] shadowSpriteArray;

    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        sheepBehaviour = sheep.GetComponent<SheepBehaviour>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        float height = sheepBehaviour.SheepHeight;
        transform.localPosition = new Vector3(0, height * -Time.deltaTime, 0);
        spriteRenderer.sprite = shadowSpriteArray[(int)Mathf.Clamp(height / 20.0f, 0.0f, 3.0f)];
    }
}
