﻿using UnityEngine;
using System.Collections;

public class PropSort : MonoBehaviour
{
    public Transform sortOrigin;
    //public int Offset;

    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Transform t = sortOrigin ? sortOrigin : transform;
       // transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, t.localPosition.y + 17.0f);
        //transform.position = new Vector3(transform.localPosition.x, transform.localPosition.y, Offset);
        spriteRenderer.sortingOrder = (int)(t.localPosition.y * -10.0f);
    }
}
