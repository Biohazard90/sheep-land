﻿using UnityEngine;
using System.Collections;

public class SheepBaaBehaviour : MonoBehaviour
{

    public AudioSource[] baaSounds;
    public AudioSource jumpSound;
    private float pitch;

    void Start()
    {
        pitch = Random.value * 0.5f + 0.75f;
    }

    public void PlayBaa()
    {
        int index = Random.Range(0, baaSounds.Length);
        AudioSource audio = baaSounds[index];
        audio.pitch = pitch;
        UpdateStereoPan(audio);
        SoundHelper.PlaySound(audio);
    }

    public void PlayJump()
    {
        UpdateStereoPan(jumpSound);
        SoundHelper.PlaySound(jumpSound);
    }

    private void UpdateStereoPan(AudioSource audio)
    {
        audio.panStereo = GetComponent<Transform>().position.x / 20.0f;
    }
}
