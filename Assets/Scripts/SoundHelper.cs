﻿using UnityEngine;
using System.Collections;

public static class SoundHelper
{
    public static void PlaySound(AudioSource sound)
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            object[] args = new object[] { "sounds/" + sound.clip.name + ".wav", sound.panStereo };
            Application.ExternalCall("playJSSound", args);
        }
        else
        {
            sound.Play();
        }
    }
}
