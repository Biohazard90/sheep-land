﻿using UnityEngine;
using System.Collections;

public class CameraSideCollider : MonoBehaviour {

    private float aspect;
    public bool Flip;

	// Use this for initialization
	void FixedUpdate () {
        if (aspect != Camera.main.aspect)
        {
            aspect = Camera.main.aspect;
            float dist = Camera.main.orthographicSize * aspect;
            if (Flip)
            {
                dist *= -1.0f;
            }
            transform.localPosition = new Vector3(dist, 0, 0);
        }
	}
}
