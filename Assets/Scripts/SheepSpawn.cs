﻿using UnityEngine;
using System.Collections;

public class SheepSpawn : MonoBehaviour
{

    public GameObject SheepPrefab;

    void OnMouseDown()
    {
        Vector3 pos = transform.position;
        Vector3 dir = new Vector3(Random.value * 2.0f - 1.0f, Random.value * 2.0f - 1.0f, transform.position.z);

        dir.Normalize();
        pos += dir * 3.0f;

        GameObject go = (GameObject)Instantiate(SheepPrefab, pos, transform.rotation);
        SheepBehaviour sheep = go.GetComponent<SheepBehaviour>();
        sheep.RandomizeDirection();

        GameObject.FindGameObjectWithTag("spawnfx").GetComponent<SpawnHintFX>().StopFX();
    }
}
