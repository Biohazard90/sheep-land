﻿using UnityEngine;
using System.Collections;

public class SplashBehaviour : MonoBehaviour
{
    public AudioSource[] splashes;

    private IEnumerator KillOnAnimationEnd()
    {
        yield return new WaitForSeconds(0.8f);
        Destroy(gameObject);
    }
    public void Start()
    {
        int index = Random.Range(0, splashes.Length);
        SoundHelper.PlaySound(splashes[index]);

        StartCoroutine(KillOnAnimationEnd());
    }
}
