﻿using UnityEngine;
using System.Collections;

public class SheepBehaviour : MonoBehaviour
{
    //    <script type = "text/javascript" >

    //// Change the cursor to a hand
    //function cursor_hand()
    //    {
    //        document.body.style.cursor = "pointer";
    //    }

    //    // Return the cursor to the default pointer
    //    function cursor_normal()
    //    {
    //        document.body.style.cursor = "default";
    //    }

    //</script>

    private Rigidbody2D rigidBody;
    private Animator animator;
    private SheepBaaBehaviour sheepBaaBehaviour;
    private PropSort propSort;
    private SpriteRenderer spriteRenderer;

    private Vector3 lastMousePosition;

    private bool isBeingDragged;
    private bool isMouseDown;
    private float height;
    private Vector3 velocity3D;
    private float suppressVelocity;

    public float SheepHeight { get { return height; } }
    public GameObject SplashPrefab;
    public bool Flipped;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sheepBaaBehaviour = GetComponentInChildren<SheepBaaBehaviour>();
        propSort = GetComponentInChildren<PropSort>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        ResetAI();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isBeingDragged)
        {
            bool wasInAir = height > 0.0f;
            velocity3D.z -= 1.0f;
            height += velocity3D.z;

            if (height <= 0.0f)
            {
                height = 0.0f;
                float speed = velocity3D.magnitude;
                if (speed > 10.0f)
                {
                    velocity3D.x *= 0.5f;
                    velocity3D.y *= 0.5f;

                    velocity3D.z *= -0.7f;
                    height = 0.01f;
                    sheepBaaBehaviour.PlayJump();
                }
                else
                {
                    if (wasInAir)
                    {
                        sheepBaaBehaviour.PlayJump();
                    }

                    velocity3D.z = 0.0f;
                    velocity3D.x *= 0.2f;
                    velocity3D.y *= 0.2f;
                }
            }

            rigidBody.velocity = new Vector2(velocity3D.x, velocity3D.y + velocity3D.z);

            if (!wasInAir && height == 0.0f)
            {
                FixedUpdateAI();
            }
        }
        else
        {
            rigidBody.velocity = Vector2.zero;
        }

        float speed2D = height > 0.0f ? 0.0f : rigidBody.velocity.magnitude;
        animator.SetFloat("Speed", speed2D);
    }

    void OnMouseEnter()
    {
        Application.ExternalCall("window.overridemouse", new object[] { true });
    }

    void OnMouseExit()
    {
        Application.ExternalCall("window.overridemouse", new object[] { false });
    }

    void OnMouseDown()
    {
        isMouseDown = true;
        lastMousePosition = Input.mousePosition;
        lastMousePosition = Camera.main.ScreenToWorldPoint(lastMousePosition);
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 5.0f);
    }

    void OnMouseUp()
    {
        propSort.enabled = true;
        if (isMouseDown)
        {
            isMouseDown = false;
            sheepBaaBehaviour.PlayBaa();
            animator.SetTrigger("Baa");
            suppressVelocity = 1.0f;
            return;
        }
        SetState(SheepState.Idle);
        stateUpdateTime = Mathf.Max(1.5f, stateUpdateTime);

        Vector3 currentMousePosition = Input.mousePosition;
        currentMousePosition = Camera.main.ScreenToWorldPoint(currentMousePosition);
        Vector3 delta = currentMousePosition - lastMousePosition;

        delta *= 10;
        float speed = delta.magnitude * 0.2f;
        velocity3D = new Vector3(delta.x, delta.y, speed);
        height = 50.0f;
        isBeingDragged = false;
        animator.SetBool("Drag", false);
    }

    void OnMouseDrag()
    {
        Vector3 currentMousePosition = Input.mousePosition;
        currentMousePosition = Camera.main.ScreenToWorldPoint(currentMousePosition);
        if (isMouseDown)
        {
            Vector3 delta = currentMousePosition - lastMousePosition;
            if (delta.magnitude > 0.125f)
            {
                propSort.enabled = false;
                spriteRenderer.sortingOrder = -99999;
                isMouseDown = false;
                SetState(SheepState.Idle);
            }
            else
            {
                return;
            }
        }
        lastMousePosition = currentMousePosition;

        height = 50.0f;
        isBeingDragged = true;
        animator.SetBool("Drag", true);

        rigidBody.MovePosition(new Vector2(currentMousePosition.x, currentMousePosition.y));
    }


    enum SheepState
    {
        Idle = 0,
        Walk = 1,
        Eat = 2
    }

    private float stateUpdateTime;
    private SheepState state = SheepState.Idle;

    private void ResetAI()
    {
        state = SheepState.Idle;
        stateUpdateTime = Random.Range(1, 5);
    }

    private void SetState(SheepState newState)
    {
        state = newState;
        animator.SetInteger("State", (int)state);
    }

    private void FixedUpdateAI()
    {
        stateUpdateTime -= Time.fixedDeltaTime;
        if (stateUpdateTime < 0.0f)
        {
            stateUpdateTime = Random.Range(1, 5);
            SetState((SheepState)Random.Range(0, System.Enum.GetNames(typeof(SheepState)).Length));

            if (state == SheepState.Walk)
            {
                RandomizeDirection();
            }
        }

        //Debug.Log(stateUpdateTime);
        //Debug.Log(state);

        if (suppressVelocity > 0.0f)
        {
            suppressVelocity -= Time.fixedDeltaTime;
            rigidBody.velocity = Vector2.zero;
            return;
        }

        switch (state)
        {
            case SheepState.Idle:
                rigidBody.velocity = Vector2.zero;
                break;

            case SheepState.Walk:
                float direction = Flipped ? -1 : 1;
                rigidBody.velocity += new Vector2(direction * Time.fixedDeltaTime * 15.0f, 0);
                if (rigidBody.velocity.magnitude > 5.0f)
                {
                    rigidBody.velocity = rigidBody.velocity.normalized * 5.0f;
                }
                break;

            case SheepState.Eat:
                rigidBody.velocity = Vector2.zero;
                break;
        }
    }

    public void RandomizeDirection()
    {
        if (Random.value > 0.5f)
        {
            Flipped = !Flipped;
            transform.localScale = new Vector3(Flipped ? -1 : 1, 1, 1);
        }
    }

    private void Splash()
    {
        Instantiate(SplashPrefab, transform.position, transform.rotation);
        Destroy(gameObject);

        GameObject.FindGameObjectWithTag("spawnfx").GetComponent<SpawnHintFX>().StartFX();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (height <= 10.0f && !isBeingDragged && other.tag == "Water")
        {
            Splash();
        }
    }
}
