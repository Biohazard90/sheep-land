﻿using UnityEngine;
using System.Collections;

public class GlobalScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 20;
	}
}
